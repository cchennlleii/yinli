// const baseUrl = "http://127.0.0.1:3000";
// const baseUrl = "http://huitongnian.nnfeifan.xyz";
const baseUrl = "https://yinliapi.v3.lol580.com";

let showModal = false
class Request {
  baseOptions(params) {
    return {
      url: (params.baseUrl || baseUrl) + params.url,
      data: params.data,
      header: params.headers || {
        "Content-Type":
          params.contentType || "application/json; charset=utf-8", // "application/json; charset=utf-8"
      },
      method: params.method || "GET",
      dataType: params.dataType, // 返回的数据格式,默认为JSON
      responseType: params.responseType, // 响应的数据类型
    };
  }

  request(params) {
    return new Promise((resolve, reject) => {
      let options = this.baseOptions(params);
      // 请求动画
      params.load &&
        wx.showLoading({
          title: "加载中",
          mask: true,
        });
      wx.request({
        ...options,
        success(res) {
          // statusCode 网络
          if (res.statusCode === 200){
            resolve(res.data);
          }
          else if (res.statusCode === 500 || res.statusCode === 501 || res.statusCode === 502){
            // 服务端错误 一般属于后端bug
            wx.showToast({
              title: '网络错误',
              icon: 'none',
              duration: 2000
            })
            reject();
          }
          else if (res.statusCode === 401 || res.statusCode === 403){
            // 登录错误
            if (!showModal){
              showModal = true
              wx.showModal({
                content: '检测到您未登录，是否先登录?',
                showCancel: true,
                cancelText: '取消',
                cancelColor: '#000000',
                confirmText: '确定',
                confirmColor: '#3CC51F',
                success(ress) {
                  showModal = false
                  if (ress.confirm) {
                    // 获取页面栈(数组)
                    let pages = getCurrentPages();
                    // 获取当前页面栈
                    let prevpage = pages[pages.length - 1];
                    if (prevpage.options.id) {
                      wx.setStorageSync('route', '/' + prevpage.route + '?id=' + prevpage.options.id);
                    } else {
                      wx.setStorageSync('route', '/' + prevpage.route);
                    }
                    //存在就清除token
                    wx.getStorageSync('sessionId') && wx.removeStorageSync('sessionId');
                    // 关闭当前栈 跳转登录
                    wx.redirectTo({
                      url: '/pages/login/login',
                    })
                  } else if (ress.cancel) {
                    // 取消 返回上一页
                    // wx.navigateBack({
                    //   delta: 1
                    // })
                    wx.switchTab({
                      url: '/pages/index/index',
                      success: (result)=>{
                        
                      },
                      fail: ()=>{},
                      complete: ()=>{}
                    });
                  }
                }
              })
            }
         
            reject(res)
          }
          else {
            setTimeout(() => {
              wx.showToast({
                title: res.data.message,
                icon: 'none',
                duration: 2000
              })
            }, 500)
            reject(res);
          }
        },
        fail(err) {
          wx.showModal({
            title: err.data && err.data.message || err.message || "网络连接超时,请稍后重试"
          })
          if (!wx.getStorageSync("sessionId")){
            if (!showModal){
              showModal = true
              wx.showModal({
                content: '检测到您未登录，是否先登录?',
                showCancel: true,
                cancelText: '取消',
                cancelColor: '#000000',
                confirmText: '确定',
                confirmColor: '#3CC51F',
                success(ress) {
                  showModal = false
                  if (ress.confirm) {
                    // 获取页面栈(数组)
                    let pages = getCurrentPages();
                    // 获取当前页面栈
                    let prevpage = pages[pages.length - 1];
                    if (prevpage.options.id) {
                      wx.setStorageSync('route', '/' + prevpage.route + '?id=' + prevpage.options.id);
                    } else {
                      wx.setStorageSync('route', '/' + prevpage.route);
                    }
                    //存在就清除token
                    wx.getStorageSync('sessionId') && wx.removeStorageSync('sessionId');
                    // 关闭当前栈 跳转登录
                    wx.redirectTo({
                      url: '/pages/login/login',
                    })
                  } else if (ress.cancel) {
                    // 取消 返回上一页
                    // wx.navigateBack({
                    //   delta: 1
                    // })
                    wx.switchTab({
                      url: '/pages/index/index',
                      success: (result)=>{
                        
                      },
                      fail: ()=>{},
                      complete: ()=>{}
                    });
                  }
                }
              })
            }
          }
          reject(err);
        },
        complete() {
          params.load && wx.hideLoading();
        
        },
      });
    });
  }
  /**
   * 
   * @param {*} url 
   * @param {*} data  传参
   * @param {*} config  配置
   */
  post(url, data, config) {
    return this.request({
      url,
      data,
      method: 'POST',
      needToken: true,
      ...config 
    });
  }

  get(url, data, config) {
    return this.request({
      url,
      data,
      method: 'GET',
      needToken: true,
      ...config 
    });
  }

  // 文件上传
  uploadFile (params){
    return new Promise((resolve, reject) => {
      wx.showLoading({
        title: '上传中',
        mask: true
      })
      wx.uploadFile({
        url: (params.baseUrl || baseUrl) + params.url, // 仅为示例，非真实的接口地址
        filePath: params.filePath,
        name: params.name || 'file',
        formData: params.data,
        header: {
          'Authorization': 'Bearer ' + wx.getStorageSync('token'),
          "Content-Type": "multipart/form-data"
        },
        success(res) {
          console.log(res);
          wx.hideLoading()

          if (res.statusCode == 200){
            let data = JSON.parse(res.data)
            resolve(data)
          }
          else {
            wx.showModal({
              content: res.errMsg
            })
            reject(res.errMsg)
          }
        },
        fail(error) {
          console.log(error)
          wx.hideLoading()
          if (error.responseJSON) {
            wx.showModal({
              content: error.responseJSON.message
            })
          }
          reject()
        },
        complete: function () {
          wx.hideLoading()
        }
      })
    })
  }
}

export default new Request();
