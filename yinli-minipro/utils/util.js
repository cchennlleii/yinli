"use strict";
Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.formatTime = function (date) {
    var year = date.getFullYear();
    var month = date.getMonth() + 1;
    var day = date.getDate();
    var hour = date.getHours();
    var minute = date.getMinutes();
    var second = date.getSeconds();
    return ([year, month, day].map(formatNumber).join("/") +
        " " + [hour, minute, second].map(formatNumber).join(":"));
};
var formatNumber = function (n) {
    var s = n.toString();
    return s[1] ? s : "0" + s;
};
// dateObj 是字符串
function dateFormat(dateObj, fmt) {
    dateObj = typeof dateObj === 'string' ? dateObj.replace(/-/g, '/') : dateObj
    dateObj = typeof dateObj == 'string' ?  new Date(dateObj) : dateObj
    var o = {
        "M+": dateObj.getMonth() + 1,
        "d+": dateObj.getDate(),
        "h+": dateObj.getHours(),
        "m+": dateObj.getMinutes(),
        "s+": dateObj.getSeconds(),
        "q+": Math.floor((dateObj.getMonth() + 3) / 3),
        "S": dateObj.getMilliseconds(),
    };
    if (/(y+)/.test(fmt))
        fmt = fmt.replace(RegExp.$1, (dateObj.getFullYear() + "").substr(4 - RegExp.$1.length));
    var k = '';
    for (k in o) {
        if (new RegExp("(" + k + ")").test(fmt)) {
            fmt = fmt.replace(RegExp.$1, RegExp.$1.length == 1 ? String(o[k]) : ("00" + String(o[k])).substr(("" + o[k]).length));
        }
    }
    return fmt;
}
// dateObj 是日期对象
function dateFormat2(dateObj, fmt) {
    var o = {
        "M+": dateObj.getMonth() + 1,
        "d+": dateObj.getDate(),
        "h+": dateObj.getHours(),
        "m+": dateObj.getMinutes(),
        "s+": dateObj.getSeconds(),
        "q+": Math.floor((dateObj.getMonth() + 3) / 3),
        "S": dateObj.getMilliseconds(),
    };
    if (/(y+)/.test(fmt))
        fmt = fmt.replace(RegExp.$1, (dateObj.getFullYear() + "").substr(4 - RegExp.$1.length));
    var k = '';
    for (k in o) {
        if (new RegExp("(" + k + ")").test(fmt)) {
            fmt = fmt.replace(RegExp.$1, RegExp.$1.length == 1 ? String(o[k]) : ("00" + String(o[k])).substr(("" + o[k]).length));
        }
    }
    return fmt;
}
// canvas 常用方法
//文字换行处理
function drawWrapText({
    ctx,
    text,
    fontSize,
    color,
    lineHeight,
    maxWidth,
    left,
    top,
    bold,
    rowLimit = 2 // 限制几行
}) {
    let context = ctx
    var chr = text.split(""); //这个方法是将一个字符串分割成字符串数组
    var temp = "";
    var row = [];
    context.font = 'normal ' + bold + ' ' + parseInt(fontSize) + 'px Source Han Sans CN';
    context.setFillStyle(color)
    for (var a = 0; a < chr.length; a++) {
        if (context.measureText(temp).width < maxWidth) {
            temp += chr[a];
        } else {
            a--; //这里添加了a-- 是为了防止字符丢失，效果图中有对比
            row.push(temp);
            temp = "";
        }
    }
    row.push(temp);

    //如果数组长度大于2 则截取前两个
    if (row.length > rowLimit) {
        var rowCut = row.slice(0, rowLimit);
        var rowPart = rowCut[rowLimit - 1];
        var test = "";
        var empty = [];
        for (var a = 0; a < rowPart.length; a++) {
            if (context.measureText(test).width < maxWidth) {
                test += rowPart[a];
            } else {
                break;
            }
        }
        empty.push(test);
        var group = empty[0] + "..." //这里只显示两行，超出的用...表示
        rowCut.splice(rowLimit - 1, 1, group);
        row = rowCut;
    }
    for (var b = 0; b < row.length; b++) {
        context.fillText(row[b], left, top + b * lineHeight);
    }
    return row
}
// 画圆形的图片
function drawCircleImg(ctx, img, x, y, r) {
    ctx.save();
    var d = 2 * r;
    var cx = x + r;
    var cy = y + r;
    ctx.beginPath()
    ctx.arc(cx, cy, r, 0, 2 * Math.PI, false);
    ctx.clip();
    ctx.drawImage(img, x, y, d, d);
    ctx.restore();
}
// 画虚线
/**
 * 
 * @param {*} ctx 
 * @param {*} pattern 详细见 setLineDash参数 
 * @param {*} offset 
 * @param {*} x1 开始点x
 * @param {*} y1 开始点y
 * @param {*} x2 结束点x
 * @param {*} y2 结束点y
 */
function drawDash(ctx, pattern, offset, x1, y1, x2, y2, color = '#76b2d9') {
    ctx.setStrokeStyle(color)
    ctx.setLineWidth(1)
    ctx.setLineDash(pattern, offset);
    ctx.beginPath();
    ctx.moveTo(x1, y1);
    ctx.lineTo(x2, y2);
    ctx.stroke();
}
// 画带圆角的矩形
function drawRoundRect(ctx, x, y, w, h, r, c = '#fff') {
    if (w < 2 * r) {
        r = w / 2;
    }
    if (h < 2 * r) {
        r = h / 2;
    }

    ctx.beginPath();
    ctx.fillStyle = c;

    ctx.arc(x + r, y + r, r, Math.PI, Math.PI * 1.5);
    ctx.moveTo(x + r, y);
    ctx.lineTo(x + w - r, y);
    ctx.lineTo(x + w, y + r);

    ctx.arc(x + w - r, y + r, r, Math.PI * 1.5, Math.PI * 2);
    ctx.lineTo(x + w, y + h - r);
    ctx.lineTo(x + w - r, y + h);

    ctx.arc(x + w - r, y + h - r, r, 0, Math.PI * 0.5);
    ctx.lineTo(x + r, y + h);
    ctx.lineTo(x, y + h - r);

    ctx.arc(x + r, y + h - r, r, Math.PI * 0.5, Math.PI);
    ctx.lineTo(x, y + r);
    ctx.lineTo(x + r, y);

    ctx.fill();
    ctx.closePath();
}

/**
 * 把秒数转化为天、时、分、秒
 * 参数value是秒数
 */
function formatSeconds(value) {
    var secondTime = parseInt(value) // 秒
    var minuteTime = 0 // 分
    var hourTime = 0 // 小时
    var dayTime = 0 // 天
    var result = ''
    if (value < 60) {
        if (value < 10){
            result = '00:0' + secondTime + ''
        }
        else {
            result = '00:' + secondTime + ''
        }
       
    } else {
        if (secondTime >= 60) { // 如果秒数大于60，将秒数转换成整数
            // 获取分钟，除以60取整数，得到整数分钟
            minuteTime = parseInt(secondTime / 60)
            // 获取秒数，秒数取佘，得到整数秒数
            secondTime = parseInt(secondTime % 60)
            // 如果分钟大于60，将分钟转换成小时
            if (minuteTime >= 60) {
                // 获取小时，获取分钟除以60，得到整数小时
                hourTime = parseInt(minuteTime / 60)
                // 获取小时后取佘的分，获取分钟除以60取佘的分
                minuteTime = parseInt(minuteTime % 60)
                if (hourTime >= 24) {
                    // 获取天数， 获取小时除以24，得到整数天
                    dayTime = parseInt(hourTime / 24)
                    // 获取小时后取余小时，获取分钟除以24取余的分；
                    hourTime = parseInt(hourTime % 24)
                }
            }
        }
        if (secondTime >= 0) {
            secondTime = parseInt(secondTime) >= 10 ? secondTime : '0' + secondTime
            result = '' + secondTime + ''
        }
        if (minuteTime > 0) {
            minuteTime = parseInt(minuteTime) >= 10 ? minuteTime : '0' + minuteTime
            result = '' + minuteTime + ':' + result
        }
        if (hourTime = 0) {
            result = '' + parseInt(hourTime) + ':' + result
        }
        if (dayTime = 0) {
            result = '' + parseInt(dayTime) + ':' + result
        }
    }
    return result
}

// http 处理成 https
function HttpToHttps (url){
    if (url.indexOf("https") < 0){
        url = url.replace("http:", "https:");
    }
    return url
}
/**
 * 处理富文本里的图片宽度自适应
 * 1.去掉img标签里的style、width、height属性
 * 2.img标签添加style属性：max-width:100%;height:auto
 * 3.修改所有style里的width属性为max-width:100%
 * 4.去掉<br/>标签
 * @param html
 * @returns {void|string|*}
 */
function formatRichText(html) {
    let newContent = html.replace(/<img[^>]*>/gi, function (match, capture) {
        match = match.replace(/style="[^"]+"/gi, '').replace(/style='[^']+'/gi, '');
        match = match.replace(/width="[^"]+"/gi, '').replace(/width='[^']+'/gi, '');
        match = match.replace(/height="[^"]+"/gi, '').replace(/height='[^']+'/gi, '');
        return match;
    });
    newContent = newContent.replace(/style="[^"]+"/gi, function (match, capture) {
        match = match.replace(/width:[^;]+;/gi, 'max-width:100%;').replace(/width:[^;]+;/gi, 'max-width:100%;');
        return match;
    });
    newContent = newContent.replace(/<br[^>]*\/>/gi, '');
    newContent = newContent.replace(/\<img/gi, '<img style="max-width:100%;height:auto;display:block;margin-top:0;margin-bottom:0;"');
    return newContent;
}
exports.dateFormat = dateFormat;
exports.dateFormat2 = dateFormat2;
exports.drawWrapText = drawWrapText;
exports.drawCircleImg = drawCircleImg;
exports.drawDash = drawDash;
exports.drawRoundRect = drawRoundRect;
exports.formatSeconds = formatSeconds;
exports.HttpToHttps = HttpToHttps;
exports.formatRichText = formatRichText;