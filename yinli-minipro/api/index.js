/*
 * @Description: 
 * @Author: wanjikun
 * @Date: 2021-05-01 12:08:55
 * @LastEditTime: 2021-05-02 14:39:07
 * @LastEditors: wanjikun
 */
import request from '../utils/request'
import qs from '../utils/qs.min.js'
class RequestIndex {
  // 例子
  getLibraryList(params, data) {
    // params 为query  data为post参数
    return request
      .post("/api/borrow/tenancyUser/index?" + qs.stringify(params), {
        area_code: data.area_code,
        longitude: data.longitude,
        latitude: data.latitude,
        rank: data.rank,
        search: data.search,
      }, {
        load: true,
      })
  } 
  login (params, data){
    return request
      .post("/applet/AppletBase/OnLogin?" + qs.stringify(params), {
        code: data.code,
      }, {
        load: true,
      })
  }
  // 校验
  CheckWxOpenSignature (params, data){
    return request
      .post("/applet/AppletBase/CheckWxOpenSignature?" + qs.stringify(params), {
        rawData: data.rawData,
        sessionId: data.sessionId,
        signature: data.signature,
      }, {
        load: false,
      })
  }
  // 解密电话号码
  DecryptPhoneNumber (params, data){
    return request
      .post("/applet/AppletBase/DecryptPhoneNumber?" + qs.stringify(params), {
        encryptedData: data.encryptedData,
        sessionId: data.sessionId,
        iv: data.iv,
      }, {
        load: false,
      })
  }
  // 解密用户信息
  DecodeEncryptedData (params, data){
    return request
      .post("/applet/AppletBase/DecodeEncryptedData?" + qs.stringify(params), {
        encryptedData: data.encryptedData,
        sessionId: data.sessionId,
        iv: data.iv,
      }, {
        load: false,
      })
  }
  // 绑定手机号码
  BindPhone (params, data){
    return request
      .post("/applet/yinli/BindPhone?" + qs.stringify(params), {
        sessionId: data.sessionId,
      }, {
        load: false,
      })
  }
  // 获取用户信息
  QueryGuestinfo (params, data){
    return request
      .post("/applet/yinli/QueryGuestinfo?" + qs.stringify(params), {
        sessionId: data.sessionId,
      }, {
        load: false,
      })
  }

  // 签到
  Sign (params, data){
    return request
      .post("/applet/yinli/Sign?" + qs.stringify(params), {
        sessionId: data.sessionId,
        lat: data.lat,
        lng: data.lng,
      }, {
        load: true,
      })
  }
  GetIsOpen(params, data) {
    return request
      .post("/applet/yinli/GetIsOpen?" + qs.stringify(params), {
      }, {
        load: true,
      })
  }

  // 修改信息 
  Edituserinfo(params, data) {
    return request
      .post("/applet/yinli/Edituserinfo?" + qs.stringify(params), data, {
        load: true,
      })
  }
  
}
export default new RequestIndex()