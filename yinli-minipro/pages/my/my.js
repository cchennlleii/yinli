/*
 * @Description: 
 * @Author: wanjikun
 * @Date: 2021-05-01 12:08:55
 * @LastEditTime: 2021-05-02 15:14:01
 * @LastEditors: wanjikun
 */
// logs.js
const util = require('../../utils/util.js')
import request from '../../api/index'
import Toast from '../../vant/toast/toast.js';
Page({
  data: {
    userInfo: {},
    host: '',
    isShow1: false, // 请于活动当天签到
    message: ''
  },
  onShow() {
    let sessionId = wx.getStorageSync('sessionId')
    if (!sessionId) {
      wx.showModal({
        title: '提示',
        content: '检测到您未登录，是否先登录?',
        showCancel: true,
        cancelText: '取消',
        cancelColor: '#000000',
        confirmText: '确定',
        confirmColor: '#3CC51F',
        success: (result) => {
          if (result.confirm) {
            wx.navigateTo({
              url: '/pages/login/login',
              success: (result) => {

              },
              fail: () => {},
              complete: () => {}
            });
          }
        },
        fail: () => {},
        complete: () => {}
      });
    }
    else {
      this.getUserInfo()
    }
  },
  onLoad (){
    if (__wxConfig.envVersion == 'develop') {
      this.setData({
        host: 'http://192.168.124.8:8080/'
      })
    }
    else {
      this.setData({
        host: 'https://yinliapi.v3.lol580.com/index.html'
      })
    }
  },
  getUserInfo (){
    request.QueryGuestinfo({}, {sessionId: wx.getStorageSync('sessionId')}).then(res => {
      if (res.code == 200){
        this.setData({
          userInfo: {...res.data, genderStr: this.getGender(res.data.gender)}
        })
      }
      else if (res.code == 'nologin'){
        wx.removeStorageSync('seesionId')
        wx.navigateTo({
          url: '/pages/login/login',
          success: (result)=>{
            
          },
          fail: ()=>{},
          complete: ()=>{}
        });
      }
      else {
        this.setData({
          isShow1: true,
          message: res.msg
        })
      }
    })
  },
  toYinshua() {
    // 印刷厂
    wx.navigateToMiniProgram({
        appId: 'wxca8b48d1172f3712',
        path: '',
        extraData: {
            foo: 'bar'
        },
        envVersion: 'release',
        success(res) {
            // 打开成功
        }
    })
  },
  getGender (gender){
    if (gender == 1){
      return '男'
    }
    else if(gender == 2) {
      return '女'
    }
    else {
      return '未知'
    }
  },
  modifyCompany (){
    wx.navigateTo({
      url: '/pages/companymodify/companymodify',
      success: (result)=>{
        
      },
      fail: ()=>{},
      complete: ()=>{}
    });
  },
  modifyBirthday (){
    wx.navigateTo({
      url: '/pages/birthdaymodify/birthdaymodify',
      success: (result)=>{
        
      },
      fail: ()=>{},
      complete: ()=>{}
    });
  },
  modifyGuestname (){
    if (this.data.userInfo.managephone){
      return
    }
    wx.navigateTo({
      url: '/pages/modifyGuestname/modifyGuestname',
      success: (result)=>{
        
      },
      fail: ()=>{},
      complete: ()=>{}
    });
  },
  modifyAddress (){
    // wx.navigateTo({
    //   url: '/pages/modifyAddress/modifyAddress',
    //   success: (result)=>{
        
    //   },
    //   fail: ()=>{},
    //   complete: ()=>{}
    // });
    let url = encodeURI(this.data.host + '#/address');
    url += ('?telephone=' + this.data.userInfo.telphone)
    url = encodeURIComponent(url)
    wx.navigateTo({
      url: '/pages/webview/webview?webViewSrc=' + url + '&isAddress=1',
      success: (result) => {

      },
      fail: () => {},
      complete: () => {}
    });
  },
  modifyMangephone (){
    if (this.data.userInfo.managephone){
      return
    }
    wx.navigateTo({
      url: '/pages/modifyMangephone/modifyMangephone',
      success: (result)=>{
        
      },
      fail: ()=>{},
      complete: ()=>{}
    });
  },
  guestNameModify() {
    wx.navigateTo({
      url: '/pages/guestNameModify/guestNameModify',
      success: (result)=>{
        
      },
      fail: ()=>{},
      complete: ()=>{}
    });
  },
  toPdf (){
    // let url = encodeURI( 'http://10.2.14.221:8080/#/pdf');
    // wx.navigateTo({
    //   url: '/pages/webview/webview?webViewSrc=' + url,
    //   success: (result)=>{
        
    //   },
    //   fail: ()=>{},
    //   complete: ()=>{}
    // });
    // wx.showLoading({
    //   icon:'none',
    //   title: "文件较大,请耐心等待",
    //   mask: true,
    // });
    Toast.loading({
      message: '文件较大\n请耐心等待...',
      forbidClick: true,
      duration: 0,
    });
    wx.downloadFile({
      // url: 'https://yinliapi.v3.lol580.com/files/static/20210412.pdf' ,
      url: 'https://ferrariwebwang.oss-cn-shanghai.aliyuncs.com/other/20210412.pdf',
      filePath:wx.env.USER_DATA_PATH+'/210412印力2020商业报告.pdf',
      success: function (res) {
        Toast.clear();
        var Path = res.filePath              //返回的文件临时地址，用于后面打开本地预览所用
        wx.openDocument({
          filePath: Path,
          success: function (res) {
            console.log('打开成功');
          }
        })
      },
      fail: function (res) {
        Toast.clear();
        console.log(res);
      }
    })
  },
  show1Close() {
    this.setData({
      isShow1: false,
    });
  },
  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  onShareTimeline (){}
})
