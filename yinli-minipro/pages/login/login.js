/*
 * @Description: 
 * @Author: wanjikun
 * @Date: 2021-05-01 13:52:26
 * @LastEditTime: 2021-05-02 14:25:18
 * @LastEditors: wanjikun
 */

const util = require('../../utils/util.js')
import request from '../../api/index'
Page({
  data: {
    userInfo: {},
    code: '',
    isToSign: false
  },
  onLoad(options) {
    if (options.isToSign) {
      this.setData({
        isToSign: true
      })
    }
  },
  cancelLogin() {
    wx.navigateBack({
      delta: 1
    });
  },
  toLogin() {
    wx.getUserProfile({
      desc: '展示用户信息', // 声明获取用户个人信息后的用途，后续会展示在弹窗中，请谨慎填写
      success: (res1) => {
        console.log(res1)
        this.setData({
          userInfo: res1.userInfo,
        })
        wx.login({
          timeout: 10000,
          success: (result) => {
            console.log(result)
            this.setData({
              code: result.code,
            })
            request.login({}, {
              code: result.code
            }).then((res) => {
              if (res.code == 200) {
                wx.setStorageSync('tempSessionId', res.data);
                // 校验
                request.CheckWxOpenSignature({}, {
                  rawData: res1.rawData,
                  sessionId: res.data,
                  signature: res1.signature
                }).then((res2) => {
                  console.log(res1.rawData)
                  //解密用户信息
                  if (res2.code == 200) {
                    request.DecodeEncryptedData({}, {
                      encryptedData: res1.encryptedData,
                      sessionId: res.data,
                      iv: res1.iv
                    }).then(() => {
                      
                       let url = "/pages/getphone/getphone"
                       if (this.data.isToSign) {
                         url = url + '?isToSign=1'
                       }

                      wx.navigateTo({
                        url: url,
                        success: (result) => {
      
                        },
                        fail: () => {},
                        complete: () => {}
                      });
                    })
                  }

                })
               
              } else {
                wx.showToast({
                  title: '登录失败',
                  icon: 'error',
                  duration: 2000
                })
              }
            })
          },
          fail: () => {},
          complete: () => {}
        });
      },
      fail: err => {
        console.log(err);
      }
    })
  }
})