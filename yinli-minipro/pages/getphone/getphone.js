/*
 * @Description: 
 * @Author: wanjikun
 * @Date: 2021-05-01 13:52:26
 * @LastEditTime: 2021-05-02 14:36:38
 * @LastEditors: wanjikun
 */

const util = require('../../utils/util.js')
import request from '../../api/index'
Page({
  data: {
    isToSign: false
  },
  onLoad(options) {
      if (options.isToSign) {
        this.setData({
          isToSign: true
        })
      }
  },
  cancelLogin (){
    wx.navigateBack({
      delta: 2
    });
  },
  getPhoneNumber (e){
    let _this = this
    if (e.detail.errMsg === 'getPhoneNumber:ok'){
      request.DecryptPhoneNumber({}, {
        iv: e.detail.iv, encryptedData: e.detail.encryptedData, sessionId: wx.getStorageSync('tempSessionId')
      }).then(res => {
        if (res.code == 200){
          request.BindPhone({}, {
            sessionId: wx.getStorageSync('tempSessionId')
          })
          wx.navigateBack({
            delta: 2
          });
          let sessionId = wx.getStorageSync('tempSessionId');
          wx.setStorageSync('telephone', res.data);
          wx.setStorageSync('sessionId', sessionId);
        }
        else {
          wx.showToast({
            title: '获取失败',
            icon: 'error',
            duration: 2000
          })
        }
      })
    }
  }
})
