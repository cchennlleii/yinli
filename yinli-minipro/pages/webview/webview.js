const util = require('../../utils/util.js')

Page({
  data: {
    webViewSrc: ''
  },
  onLoad(options) {
    console.log(options.webViewSrc);
    if (options.webViewSrc) {
      if (options.isAddress) {
        this.setData({
          webViewSrc: decodeURIComponent(options.webViewSrc)
        })
      }
      else {
        this.setData({
          webViewSrc: options.webViewSrc
        })
      }
      
    }
  },
  dealMsg (e){
  },
  toYinshua() {
    // 印刷厂
    wx.navigateToMiniProgram({
        appId: 'wxca8b48d1172f3712',
        path: '',
        extraData: {
            foo: 'bar'
        },
        envVersion: 'release',
        success(res) {
            // 打开成功
        }
    })
  },
})
