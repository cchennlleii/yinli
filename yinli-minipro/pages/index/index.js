import request from "../../api/index";

// 获取应用实例
const app = getApp();
let timer = null;
Page({
  data: {
    motto: "Hello World",
    userInfo: {},
    hasUserInfo: false,
    canIUse: wx.canIUse("button.open-type.getUserInfo"),
    canIUseGetUserProfile: false,
    canIUseOpenData:
      wx.canIUse("open-data.type.userAvatarUrl") &&
      wx.canIUse("open-data.type.userNickName"), // 如需尝试获取用户信息可改为false
    sessionId: "",
    host: "",
    hotelAddress: {
      long: "104.02330730664063", // 经度
      lanti: "30.571491879255767", // 维度
      // long: 106.498197, // 经度
      // lanti: 29.628356, // 维度
      address: "成都w酒店",
    },
    isToSign:false,
    latitude: "",
    longitude: "",
    isShow1: false, // 请于活动当天签到
    isShow2: false, //
    isShow3: false, // 手册
    isShow4: false, // 照片
    isShow5: false, // 签到
    isShow6: false, // 直播二维码
    liveQrcodeSrc: "https://yinliapi.v3.lol580.com/files/static/live.png",
    shansuoStatus: {
      btn1: false,
      btn2: false,
      btn3: false,
      btn4: false,
      btn5: false,
      btn6: false,
      btn7: false,
    },
    userInfo: {},
  },
  // 事件处理函数
  bindViewTap() {
    wx.navigateTo({
      url: "../logs/logs",
    });
  },

  getLocation() {
    return new Promise((resolve, reject) => {
      let _this = this;
      let vm = this;
      wx.getSetting({
        success: (res) => {
          // res.authSetting['scope.userLocation'] == undefined    表示 初始化进入该页面
          // res.authSetting['scope.userLocation'] == false    表示 非初始化进入该页面,且未授权
          // res.authSetting['scope.userLocation'] == true    表示 地理位置授权
          // 拒绝授权后再次进入重新授权
          if (
            res.authSetting["scope.userLocation"] != undefined &&
            res.authSetting["scope.userLocation"] != true
          ) {
            // console.log('authSetting:status:拒绝授权后再次进入重新授权', res.authSetting['scope.userLocation'])
            wx.showModal({
              title: "",
              content: "【IN友会】需要获取你的地理位置，请确认授权",
              success: function (res) {
                if (res.cancel) {
                  wx.showToast({
                    title: "拒绝授权",
                    icon: "none",
                  });
                } else if (res.confirm) {
                  wx.openSetting({
                    success: function (dataAu) {
                      // console.log('dataAu:success', dataAu)
                      if (dataAu.authSetting["scope.userLocation"] == true) {
                        //再次授权，调用wx.getLocation的API
                        wx.getLocation({
                          type: "gcj02",
                          altitude: false,
                          success: (result) => {
                            console.log(result);
                            // 腾讯位置服务
                            this.setData({
                              latitude: result.latitude,
                              longitude: result.longitude,
                            });
                            resolve();
                          },
                          fail: () => {
                            reject();
                          },
                          complete: () => {},
                        });
                      } else {
                        wx.showToast({
                          title: "授权失败",
                          icon: "none",
                        });
                      }
                    },
                  });
                }
              },
            });
          }
          // 初始化进入，未授权
          else if (res.authSetting["scope.userLocation"] == undefined) {
            // console.log('authSetting:status:初始化进入，未授权', res.authSetting['scope.userLocation'])
            //调用wx.getLocation的API
            wx.getLocation({
              type: "gcj02",
              altitude: false,
              success: (result) => {
                console.log(result);
                // 腾讯位置服务
                this.setData({
                  latitude: result.latitude,
                  longitude: result.longitude,
                });
                resolve();
              },
              fail: () => {
                reject();
              },
              complete: () => {},
            });
          }
          // 已授权
          else if (res.authSetting["scope.userLocation"]) {
            // console.log('authSetting:status:已授权', res.authSetting['scope.userLocation'])
            //调用wx.getLocation的API
            wx.getLocation({
              type: "gcj02",
              altitude: false,
              success: (result) => {
                console.log(result);
                // 腾讯位置服务
                this.setData({
                  latitude: result.latitude,
                  longitude: result.longitude,
                });
                resolve();
              },
              fail: (err) => {
                 wx.showToast({
                   title: "请勿频繁点击",
                   icon: "none",
                   image: "",
                   duration: 1500,
                   mask: false,
                   success: (result) => {},
                   fail: () => {},
                   complete: () => {},
                 });
                reject();
              },
              complete: () => {},
            });
          }
        },
      });
    });
  },

  toSign() {
    // 判断登录
    let sessionId = wx.getStorageSync("sessionId");
    if (!sessionId) {
      return wx.showModal({
        title: "提示",
        content: "检测到您未登录，是否先登录?",
        showCancel: true,
        cancelText: "取消",
        cancelColor: "#000000",
        confirmText: "确定",
        confirmColor: "#3CC51F",
        success: (result) => {
          if (result.confirm) {
            wx.navigateTo({
              url: "/pages/login/login",
              success: (result) => {},
              fail: () => {},
              complete: () => {},
            });
          }
        },
        fail: () => {},
        complete: () => {},
      });
    }
    // 判断时候
    this.isActivityOpen().then((code) => {
      if (code == 200) {
        // 判断位置
        this.getLocation().then(() => {
          // 计算位置
          let distance = this.countDistance(
            Number(this.data.latitude),
            Number(this.data.longitude),
            Number(this.data.hotelAddress.lanti),
            Number(this.data.hotelAddress.long)
          );
          if (distance > 20) {
            // 不在距离范围内
            wx.showToast({
              title: "请于酒店附近签到",
              icon: "none",
              image: "",
              duration: 1500,
              mask: false,
              success: (result) => {},
              fail: () => {},
              complete: () => {},
            });
          } else {
            // 调用签到接口
            request
              .Sign(
                {},
                {
                  sessionId: wx.getStorageSync("sessionId"),
                  lat: this.data.latitude + '',
                  lng: this.data.longitude + '',
                }
              )
              .then((res) => {
                if (res.code == 200) {
                  // 签到成功
                  this.setData({
                    isShow5: true,
                  });
                } else if (res.code == "nologin") {
                  wx.removeStorageSync("seesionId");
                  wx.navigateTo({
                    url: "/pages/login/login",
                    success: (result) => {},
                    fail: () => {},
                    complete: () => {},
                  });
                } else {
                  wx.showToast({
                    title: res.msg,
                    icon: "none",
                    image: "",
                    duration: 1500,
                    mask: false,
                    success: (result) => {},
                    fail: () => {},
                    complete: () => {},
                  });
                }
              });
          }
        });
      } else {
        this.setData({
          isShow1: true,
        });
      }
    });
  },
  isActivityOpen() {
    return new Promise((resolve, reject) => {
      request.GetIsOpen({}, {}).then((res) => {
        resolve(res.code);
      });
    });
  },
  countDistance(lat1, lng1, lat2, lng2) {
    var EARTH_RADIUS = 6378137.0; // 单位M
    var PI = Math.PI;

    function getRad(d) {
      return (d * PI) / 180.0;
    }

    var f = getRad((lat1 + lat2) / 2);
    var g = getRad((lat1 - lat2) / 2);
    var l = getRad((lng1 - lng2) / 2);

    var sg = Math.sin(g);
    var sl = Math.sin(l);
    var sf = Math.sin(f);

    var s, c, w, r, d, h1, h2;
    var a = EARTH_RADIUS;
    var fl = 1 / 298.257;

    sg = sg * sg;
    sl = sl * sl;
    sf = sf * sf;

    s = sg * (1 - sl) + (1 - sf) * sl;
    c = (1 - sg) * (1 - sl) + sf * sl;

    w = Math.atan(Math.sqrt(s / c));
    r = Math.sqrt(s * c) / w;
    d = 2 * w * a;
    h1 = (3 * r - 1) / 2 / c;
    h2 = (3 * r + 1) / 2 / s;
    let result = d * (1 + fl * (h1 * sf * (1 - sg) - h2 * (1 - sf) * sg));
    return (result / 1000).toFixed(2);
  },
  toZhuozhuang() {
    let url = encodeURI(this.data.host + "#/zhaozhuang");
    wx.navigateTo({
      url: "/pages/webview/webview?webViewSrc=" + url,
      success: (result) => {},
      fail: () => {},
      complete: () => {},
    });
  },
  // 已解决
  toMoments() {
    this.isActivityOpen().then((code) => {
      if (code == 200 || code == 500) {
        // 显示相册
        wx.navigateTo({
          url:
            "/pages/webview/webview?webViewSrc=" +
            encodeURI("https://live.photoplus.cn/live/43822956"),
          success: (result) => {},
          fail: () => {},
          complete: () => {},
        });
      } else {
        this.setData({
          isShow4: true,
        });
      }
    });
  },
  // 生命力手册
  toHandlebook() {
    // isShow3
    // 生命力手册判断的是5.28前和后
    let currentTimeStamp = new Date().getTime();
    if (currentTimeStamp < new Date("2021/5/28 00:00:00").getTime()) {
      this.setData({
        isShow3: true,
      });
    } else {
      let url = encodeURI(this.data.host + "#/handlebook");
      wx.navigateTo({
        url: "/pages/webview/webview?webViewSrc=" + url,
        success: (result) => {},
        fail: () => {},
        complete: () => {},
      });
    }
  },
  toShop() {
    // 印刷厂
    wx.navigateToMiniProgram({
      appId: "wxca8b48d1172f3712",
      path: "",
      envVersion: "release",
      success(res) {
        // 打开成功
      },
    });
  },
  toYinshua() {
    // 印刷厂
    let url = encodeURI(this.data.host + "#/video");
    wx.navigateTo({
      url: "/pages/webview/webview?webViewSrc=" + url,
      success: (result) => {},
      fail: () => {},
      complete: () => {},
    });
  },
  toLive() {
    // 直播
    this.setData({
      isShow6: true,
    });
  },
  onLoad(oprions) {
    if (oprions.isToSign) {
      this.setData({
        isToSign:true
      })
    }
    if (wx.getUserProfile) {
      this.setData({
        canIUseGetUserProfile: true,
      });
    }
    if (__wxConfig.envVersion == "develop") {
      this.setData({
        host: "http://10.2.14.221:8080/",
      });
    } else {
      this.setData({
        host: "https://yinliapi.v3.lol580.com/index.html",
      });
    }

    clearInterval(timer);
    timer = setInterval(() => {
      this.getShansuoStatus();
    }, 1000);
    // this.getLocation()
  },
  onShow() {
    let sessionId = wx.getStorageSync("sessionId");
    if (!sessionId) {
      let url = "/pages/login/login"
      if (this.data.isToSign){
        url = url + '?isToSign=1'
      }
      wx.showModal({
        title: "提示",
        content: "检测到您未登录，是否先登录?",
        showCancel: true,
        cancelText: "取消",
        cancelColor: "#000000",
        confirmText: "确定",
        confirmColor: "#3CC51F",
        success: (result) => {
          if (result.confirm) {
            wx.navigateTo({
              url: url,
              success: (result) => {},
              fail: () => {},
              complete: () => {},
            });
          }
        },
        fail: () => {},
        complete: () => {},
      });
    } else {
      this.getUserInfo();
      if (this.data.isToSign){
        this.toSign()
        this.setData({
          isToSign: false
        })
      }
    }
  },
  getUserInfo() {
    request
      .QueryGuestinfo(
        {},
        {
          sessionId: wx.getStorageSync("sessionId"),
        }
      )
      .then((res) => {
        if (res.code == 200) {
          this.setData({
            userInfo: {
              ...res.data,
            },
          });
        } else if (res.code == "nologin") {
          wx.removeStorageSync("seesionId");
          wx.navigateTo({
            url: "/pages/login/login",
            success: (result) => {},
            fail: () => {},
            complete: () => {},
          });
        }
      });
  },
  getShansuoStatus() {
    let currentTimeStamp = new Date().getTime();

    if (
      currentTimeStamp >= new Date("2021/5/13 00:00:00").getTime() &&
      currentTimeStamp < new Date("2021/5/13 14:00:00").getTime()
    ) {
      if (!this.data.shansuoStatus.btn1) {
        this.setData({
          "shansuoStatus.btn1": true,
        });
      }
    } else {
      this.setData({
        "shansuoStatus.btn1": false,
      });
    }

    if (currentTimeStamp < new Date("2021/5/13 00:00:00").getTime()) {
      if (!this.data.shansuoStatus.btn2) {
        this.setData({
          "shansuoStatus.btn2": true,
        });
      }
    } else {
      this.setData({
        "shansuoStatus.btn2": false,
      });
    }

    if (
      currentTimeStamp >= new Date("2021/5/13 14:00:00").getTime() &&
      currentTimeStamp < new Date("2021/5/13 14:30:00").getTime()
    ) {
      if (!this.data.shansuoStatus.btn3) {
        this.setData({
          "shansuoStatus.btn3": true,
        });
      }
    } else {
      this.setData({
        "shansuoStatus.btn3": false,
      });
    }

    if (
      (currentTimeStamp >= new Date("2021/5/13 14:30:00").getTime() &&
        currentTimeStamp < new Date("2021/5/13 15:50:00").getTime()) ||
      (currentTimeStamp >= new Date("2021/5/13 16:30:00").getTime() &&
        currentTimeStamp < new Date("2021/5/13 18:45:00").getTime())
    ) {
      if (!this.data.shansuoStatus.btn4) {
        this.setData({
          "shansuoStatus.btn4": true,
        });
      }
    } else {
      this.setData({
        "shansuoStatus.btn4": false,
      });
    }

    if (currentTimeStamp >= new Date("2021/5/28 00:00:00").getTime()) {
      if (!this.data.shansuoStatus.btn7) {
        this.setData({
          "shansuoStatus.btn7": true,
        });
      }
    } else {
      this.setData({
        "shansuoStatus.btn7": false,
      });
    }
  },
  saveImage(e) {
    let url = e.currentTarget.dataset.url;
    //用户需要授权
    this.saveToAlbum(url);
  },
  saveToAlbum(url) {
    wx.getImageInfo({
      src: url,
      success: (res) => {
        let path = res.path;
        
        wx.saveImageToPhotosAlbum({
          filePath: path,
          success: (res) => {
            console.log(res);
          },
          fail: function (err) {
            if (err.errMsg) {//重新授权弹框确认
                wx.showModal({
                  title: '提示',
                  content: '【IN友会】需要获取你的相册权限，请确认授权',
                  showCancel: false,
                  success(res) {
                    if (res.confirm) {//重新授权弹框用户点击了确定
                      wx.openSetting({//进入小程序授权设置页面
                        success(settingdata) {
                          if (settingdata.authSetting['scope.writePhotosAlbum']) {//用户打开了保存图片授权开关
                            wx.saveImageToPhotosAlbum({
                              filePath: path,
                              success: function (data) {
                                wx.showToast({
                                  title: '保存成功',
                                  icon: 'success',
                                  duration: 2000
                                })
                              },
                            })
                          } else {//用户未打开保存图片到相册的授权开关
                            wx.showModal({
                              title: '温馨提示',
                              content: '授权失败，请稍后重新获取',
                              showCancel: false,
                            })
                          }
                        }
                      })
                    } 
                  }
                })
            }
          }
        });
      },
      fail: (res) => {
        console.log(res);
      },
    });
  },
  getUserProfile(e) {
    // 推荐使用wx.getUserProfile获取用户信息，开发者每次通过该接口获取用户个人信息均需用户确认，开发者妥善保管用户快速填写的头像昵称，避免重复弹窗
    wx.getUserProfile({
      desc: "展示用户信息", // 声明获取用户个人信息后的用途，后续会展示在弹窗中，请谨慎填写
      success: (res) => {
        console.log(res);
        this.setData({
          userInfo: res.userInfo,
          hasUserInfo: true,
        });
      },
    });
  },
  toPlay() {
    wx.navigateTo({
      url: "/pages/play/play",
      success: (result) => {},
      fail: () => {},
      complete: () => {},
    });
  },
  show1Close() {
    this.setData({
      isShow1: false,
    });
  },
  show2Close() {
    this.setData({
      isShow2: false,
    });
  },
  show3Close() {
    this.setData({
      isShow3: false,
    });
  },
  show4Close() {
    this.setData({
      isShow4: false,
    });
  },
  show5Close() {
    this.setData({
      isShow5: false,
    });
  },
  show6Close() {
    this.setData({
      isShow6: false,
    });
  },
  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  onShareTimeline (){}
});
