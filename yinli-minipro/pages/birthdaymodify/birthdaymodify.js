// logs.js
const util = require('../../utils/util.js')
import request from '../../api/index'
import {
  dateFormat
} from '../../utils/util'
Page({
  data: {
    userInfo: {},
    inputValue: new Date().getTime(),
    inputValue2: '', 
    minDate: new Date('1920/01/01'),
    formatter(type, value) {
      if (type === 'year') {
        return `${value}年`;
      }
      if (type === 'month') {
        return `${value}月`;
      }
      if (type === 'day') {
        return `${value}日`;
      }
      return value;
    },
  },
  onLoad() {
    this.getUserInfo()
  },
  valueChange(e) {
    this.setData({
      inputValue: e.detail.value
    })
  },
  onInput(e) {
    this.setData({
      inputValue2: e.detail
    })
  },
  getUserInfo() {
    request.QueryGuestinfo({}, {
      sessionId: wx.getStorageSync('sessionId')
    }).then(res => {
      if (res.code == 200) {
        this.setData({
          userInfo: {
            ...res.data,
            genderStr: this.getGender(res.data.gender)
          },
          inputValue: new Date(res.data.birthday).getTime()
        })
      } else if (!res.data) {
        // wx.removeStorageSync('seesionId')
        // wx.navigateTo({
        //   url: '/pages/login/login',
        //   success: (result)=>{

        //   },
        //   fail: ()=>{},
        //   complete: ()=>{}
        // });
      } else if (res.code == 'nologin') {
        wx.removeStorageSync('seesionId')
        wx.navigateTo({
          url: '/pages/login/login',
          success: (result) => {

          },
          fail: () => {},
          complete: () => {}
        });
      }
    })
  },
  submit() {
    let birthday = dateFormat(new Date(this.data.inputValue2), 'yyyy-MM-dd')
    if (!this.data.inputValue2) {
      return wx.showToast({
        title: '请选择日期',
        icon: 'none',
        image: '',
        duration: 1500,
        mask: false,
      });
    }
    request.Edituserinfo({}, {
      birthday: birthday,
      sessionId: wx.getStorageSync('sessionId'),
    }).then(res => {
      if (res.code == 200) {
        wx.showToast({
          title: '修改成功',
          icon: 'none',
          image: '',
          duration: 1500,
          mask: false,
        });
        wx.navigateBack({
          delta: 1
        });
      } else {
        wx.showToast({
          title: res.msg,
          icon: 'none',
          image: '',
          duration: 1500,
          mask: false,
        });
      }
    })

  },
  getGender(gender) {
    if (gender == 1) {
      return '男'
    } else if (gender == 2) {
      return '女'
    } else {
      return '未知'
    }
  },
})