// logs.js
const util = require('../../utils/util.js')
import request from '../../api/index'
Page({
  data: {
    inputValue: '',
    userInfo: {}
  },
  onLoad() {
    this.getUserInfo()
  },
  valueChange (e){
    this.setData({
      inputValue: e.detail.value
    })
  },
  getUserInfo (){
    request.QueryGuestinfo({}, {sessionId: wx.getStorageSync('sessionId')}).then(res => {
      if (res.code == 200){
        this.setData({
          userInfo: {...res.data},
          inputValue: res.data.managephone
        })
      }
      else if (!res.data){
        // wx.removeStorageSync('seesionId')
        // wx.navigateTo({
        //   url: '/pages/login/login',
        //   success: (result)=>{
            
        //   },
        //   fail: ()=>{},
        //   complete: ()=>{}
        // });
      }
      else if (res.code == 'nologin'){
        wx.removeStorageSync('seesionId')
        wx.navigateTo({
          url: '/pages/login/login',
          success: (result)=>{
            
          },
          fail: ()=>{},
          complete: ()=>{}
        });
      }
    })
  },
  submit (){
    if (!this.data.inputValue){
      return wx.showToast({
        title: '请填写接待人电话',
        icon: 'none',
        image: '',
        duration: 1500,
        mask: false,
      });
    }
    request.Edituserinfo({}, {
      managephone: this.data.inputValue,
      sessionId: wx.getStorageSync('sessionId'),
    }).then(res => {
      if (res.code == 200){
        wx.showToast({
          title: '修改成功',
          icon: 'none',
          image: '',
          duration: 1500,
          mask: false,
        });
         wx.navigateBack({
           delta: 1
         });
      }
      else {
        wx.showToast({
          title: res.msg,
          icon: 'none',
          image: '',
          duration: 1500,
          mask: false,
        });
      }
    })

  },
  getGender (gender){
    if (gender == 1){
      return '男'
    }
    else if(gender == 2) {
      return '女'
    }
    else {
      return '未知'
    }
  },
})
