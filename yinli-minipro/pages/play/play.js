/*
 * @Description: 
 * @Author: wanjikun
 * @Date: 2021-05-01 12:42:51
 * @LastEditTime: 2021-05-01 15:25:09
 * @LastEditors: wanjikun
 */
// logs.js
const util = require('../../utils/util.js')

Page({
  data: {
    host: ''
  },
  onLoad() {
    if (__wxConfig.envVersion == 'develop') {
      this.setData({
        host:  'http://10.2.14.204:8080/'
      })
    }
    else {
      this.setData({
        host: 'https://yinliapi.v3.lol580.com/index.html'
      })
    }
  },
  toWebview (e){
    let property = e.currentTarget.dataset.link 
    let url = encodeURI( this.data.host + '#/' + property);
    wx.navigateTo({
      url: '/pages/webview/webview?webViewSrc=' + url,
      success: (result)=>{
        
      },
      fail: ()=>{},
      complete: ()=>{}
    });
  },
  makePhone (){
    wx.makePhoneCall({
      phoneNumber: '13902462669',
      success: (result)=>{
        
      },
      fail: ()=>{},
      complete: ()=>{}
    });
  },
  toYinshua() {
    // 印刷厂
    wx.navigateToMiniProgram({
        appId: 'wxca8b48d1172f3712',
        path: '',
        extraData: {
            foo: 'bar'
        },
        envVersion: 'release',
        success(res) {
            // 打开成功
        }
    })
  },
  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  onShareTimeline (){}
})
