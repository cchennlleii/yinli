export function appH5Links (){
  if (process.env.VUE_APP_ENV === 'production'){
    return {
      'rejister': 'https://bac.oshanauto.com/h5/movable/#/blessedInvite/index',
      'fall': 'https://bac.oushangstyle.com/h5/dist/#/common/download?channel=202093', // 落地页
      'xiading': 'https://bac.oushangstyle.com/h5/order/#/orderLive?agent=3156', // 下定
      'circle': 'https://cir.oushangstyle.com/public/dist/#/home', // 圈子
      'comment': 'https://bac.oushangstyle.com/h5/dist/#/activity/commentsAwards', // 评论
      'miaosha': 'http://hby.oushangstyle.com/fe_seckill/#/seckillv2', // 秒杀
      'sign': '',
      'verify': 'https://bac.oushangstyle.com/h5/dist/#/approve/list'
    }
  }
  else {
    // 测试环境
    return {
      'rejister': 'https://cs.leshangche.com/h5/movable/#/blessedInvite/index',
      'fall': 'https://bac.oushangstyle.com/h5/dist/#/common/download?channel=202093',
      'xiading': 'https://cs.leshangche.com/h5/order/#/orderLive?agent=3156', // 下定
      'circle': 'http://cs20cir.leshangche.com/public/dist/#/home', // 圈子
      'comment': 'https://cs.leshangche.com/h5/dist/#/activity/commentsAwards', // 评论
      'miaosha': 'http://hby.leshangche.com/fe_seckill/#/seckillv2',  // 秒杀
      'sign': '',
      'verify': 'https://cs.leshangche.com/h5/dist/#/approve/list'
    }
  }
}
export function getSystem (){
  var u = navigator.userAgent;
  // var isAndroid = u.indexOf("Android") > -1 || u.indexOf("Adr") > -1; //android终端
  var isiOS = !!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/); //ios终端
  return isiOS ? "ios" : "android";
}
export function RandomNum(Min, Max) {
  var Range = Max - Min;
  var Rand = Math.random();
  var num = Min + Math.floor(Rand * Range); //舍去
  return num;
}

export let isIPhoneX = /iphone/gi.test(window.navigator.userAgent) && window.devicePixelRatio && window.devicePixelRatio === 3 && window.screen.width === 375 && window.screen.height === 812;

export let isIPhone12Mini = /iphone/gi.test(window.navigator.userAgent) && window.devicePixelRatio && window.devicePixelRatio === 3 && window.screen.width === 390 && window.screen.height === 844;
// iPhone XS Max
export let isIPhoneXSMax = /iphone/gi.test(window.navigator.userAgent) && window.devicePixelRatio && window.devicePixelRatio === 3 && window.screen.width === 414 && window.screen.height === 896;
// iPhone XR
export let isIPhoneXR = /iphone/gi.test(window.navigator.userAgent) && window.devicePixelRatio && window.devicePixelRatio === 2 && window.screen.width === 414 && window.screen.height === 896;

export let isIPhone6p =
  /iphone/gi.test(window.navigator.userAgent) &&
  window.devicePixelRatio &&
  window.devicePixelRatio === 3 &&
  window.screen.width === 414 &&
  window.screen.height === 736


  
 // 32位随机数
 export function randomString(len) {
    len = len || 32;
   
   var $chars = 'ABCDEFGHJKMNPQRSTWXYZabcdefhijkmnprstwxyz2345678'; /****默认去掉了容易混淆的字符oOLl,9gq,Vv,Uu,I1****/
   
   var maxPos = $chars.length;
   
   var pwd = '';
   
   for (let i = 0; i < len; i++) {
      pwd += $chars.charAt(Math.floor(Math.random() * maxPos));
     
   }
   
   return pwd;
   
 }

 // 加密手机号

export function encodePhone (phone){
  // let phone =15865234562;
  
  phone = "" + phone;
  let result= phone.substr(0,3) + "****" + phone.substr(7)
  return result
}

// 判断是否是手机号码
export function isPhone (tele){
  let reg = /^[1][0-9][0-9]{9}$/
  return reg.test(tele)
}
export function addZero(num) {
  if (num < 10){
    return '0' + num
  }
  else {
    return num
  }
}
export function getYMDHMS(timestamp) {
  let day = 0, hour = 0, min = 0, sec = 0;
  let rest = 0;

  day = timestamp / (1000 * 60 * 60 * 24);
  day = day > 0 ? Math.floor(day) : 0
  rest = timestamp - day * 1000 * 60 * 60 * 24

  hour = rest / (1000 * 60 * 60);
  hour = hour > 0 ? Math.floor(hour) : 0
  rest = rest - hour * 1000 * 60 * 60

  min = rest / (1000 * 60);
  min = min > 0 ? Math.floor(min) : 0
  rest = rest - min * 1000 * 60

  sec = rest / 1000;
  sec = sec > 0 ? Math.floor(sec) : 0

  return day + '天 ' + addZero(hour) + '：' + addZero(min) + '：' + addZero(sec)
}
// 计算到当前时间的时分秒
export function deltaDateGap (timeStr){
  timeStr = timeStr ? timeStr : '2020/05/10 23:59:59'
  timeStr = timeStr.replace(/\//g, '-')
  let timeObj = new Date(timeStr)
  let gap = timeObj.getTime() - new Date().getTime()
  if (gap < 0) {
    return '活动时间已过'
  }
  else {
    return getYMDHMS(gap)
  }
}

export function dateFormat(dateObj, fmt) {
  dateObj = dateObj ? dateObj : ''
  if (!dateObj) {
    return ''
  }
  if (dateObj && typeof dateObj === 'string') {
    // dateObj = dateObj ?  dateObj.replace(/-/g, '/') : '';
    dateObj = new Date(dateObj);
  }

  var o = {
    'M+': dateObj.getMonth() + 1, //月份
    'd+': dateObj.getDate(), //日
    'h+': dateObj.getHours(), //小时
    'm+': dateObj.getMinutes(), //分
    's+': dateObj.getSeconds(), //秒
    'q+': Math.floor((dateObj.getMonth() + 3) / 3), //季度
    S: dateObj.getMilliseconds() //毫秒
  };
  if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (dateObj.getFullYear() + '').substr(4 - RegExp.$1.length));
  for (var k in o)
    if (new RegExp('(' + k + ')').test(fmt))
      fmt = fmt.replace(RegExp.$1, RegExp.$1.length == 1 ? o[k] : ('00' + o[k]).substr(('' + o[k]).length));
  return fmt;
}