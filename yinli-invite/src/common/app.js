import {
  getSystem
} from "./utils";
// 跳转app地址页面选择地区为例
export const toAppAddress = (callback) => {
  try {
    window.OSApp.addNewAddress(callback)
  } catch (error) {

    console.log('非app环境')
  }
}

//获取用户信息
export const getUserInfo = () => {
  try {
    return window.OSApp.getUserInfo()
  } catch (error) {

    console.log('非app环境')
    return {}
  }
}
export const getToken = () => {
  try {
    return window.OSApp.getToken()
  } catch (error) {

    console.log('非app环境')
    return ''
  }
}
export const isIphoneX = () => {
  try {
    return window.OSApp.isIphoneX()
  } catch (error) {
    console.log('非app环境')
    return false
  }
}

//设置title
export const setNavTitle = (title) => {
  try {
    window.OSApp.setNavTitle(title)
  } catch (error) {

    console.log('非app环境')
  }
}
// 分享
export const shareTo = (params, callbacks) => {
  try {
    console.log(params);
     //埋点
    var coper = {
      TrackEvent: function (category, action) {
        window._czc.push(["_trackEvent", category, action, "reducePrice", 1]);
      }
    }
    if (params.shareType == 1){
      coper.TrackEvent("分享", '好友分享');
    }
    else {
      coper.TrackEvent("分享", '朋友圈分享');
    }
    

    window.OSApp.shareTo(JSON.stringify(params), callbacks)
  } catch (error) {
    console.log('非app环境')
  }
}
//
export const showToast = (text) => {
  try {
    window.OSApp.showToast(text)
  } catch (error) {

    console.log('非app环境')
  }
}
// 关闭页面
export const closeView = () => {
  try {
    window.OSApp.closeView()
  } catch (error) {

    console.log('非app环境')
  }
}

// 是否在app内

export const isInApp = () => {
  try {
    return window.OSApp.isInApp()
  } catch (error) {
    return false
  }
}
// 跳转app车主认证
export const goToCarCertification = () => {
  try {
    window.OSApp.goToCarCertification()
  } catch (error) {
    console.log('非app环境')
  }
}

//显示登陆
export const showLogin = (callback) => {
  try {
    window.OSApp.showLogin(callback)
  } catch (error) {
    console.log('非app环境')
  }
}

// 打开h5页
/**
 * 
 * @param { type, value } params   type：2（H5链接） 17（车主认证） value：H5链接地址
 */
export const openPage = (params) => {
  console.log(params);
  try {
    window.OSApp.openPage(JSON.stringify(params))
  } catch (error) {
    console.log('非app环境')
  }
}

/**
 * 刷新界面
 */
export const reloadWebView = () => {
  try {
    if (getSystem() == 'ios') {
      window.OSApp.reloadWebView()
    }
    else {
      window.OSApp.reloadWebView(1)
    }
  } catch (error) {
    console.log('非app环境')
  }
}
// pay
export const openPay = (params) => {
  console.log(111);
  console.log(params);
  try {
    if (getSystem() == 'ios') {
      window.OSApp.openPay(params.payCode, params.data, params.callback)
    }
    else {
      window.OSApp.openPay(params.payCode, params.data, params.callback)
    }
  } catch (error) {
    console.log('非app环境')
  }
}
