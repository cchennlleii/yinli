import axios from 'axios';
import qs from 'qs';
import md5 from 'js-md5'

export function createXHR(baseURL) {
  const service = axios.create({
    baseURL,
    withCredentials: false,
    timeout: 9000
  });

  service.interceptors.request.use(
    config => {
      config.headers['Content-Type'] = 'application/json;charset=UTF-8';
      // config.data = qs.stringify(config.data);

      let api_token = '';
      if (api_token == false) api_token = "";
      // let time = xTimestamp(new Date());
      let time = new Date().getTime();
      let randstr = Math.random();
      let sign = '';
      if (config.data != null) {
        sign = (typeof (config.data) == 'string' ? config.data : JSON.stringify(config.data))
      }
      sign = md5(sign + time + randstr + api_token);
      config.headers["api_token"] = api_token;
      config.headers["api_time"] = time;
      config.headers["api_randstr"] = randstr;
      config.headers["api_sign"] = sign;
      return config;
    },
    error => {
      console.log(error); // for debug.
      return Promise.reject(error);
    }
  );

  service.interceptors.response.use(
    resp => {
      // TODO http异常代码处理
      // TODO 接口自定义请求异常处理
      return resp.data;
    },
    error => {
      console.log('response error: ', error); // for debug
      return Promise.reject(error);
    }
  );

  return service;
}
const service = createXHR(process.env.VUE_APP_BASE_HOST);
// 有线
// const service = createXHR('http://10.2.14.167:8080/');
// const service = createXHR('http://2280565n60.goho.co/');

export default service;
