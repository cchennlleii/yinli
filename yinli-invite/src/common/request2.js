import axios from 'axios';
import qs from 'qs';
import md5 from 'js-md5'

export function createXHR(baseURL) {
  const service = axios.create({
    baseURL,
    withCredentials: false,
    timeout: 9000
  });

  service.interceptors.request.use(
    config => {
      config.headers['Content-Type'] = 'application/x-www-form-urlencoded';
      config.data = qs.stringify(config.data);

      return config;
    },
    error => {
      console.log(error); // for debug.
      return Promise.reject(error);
    }
  );

  service.interceptors.response.use(
    resp => {
      // TODO http异常代码处理
      // TODO 接口自定义请求异常处理
      return resp.data;
    },
    error => {
      console.log('response error: ', error); // for debug
      return Promise.reject(error);
    }
  );

  return service;
}
const service = createXHR('http://h5.zegelo.com/');
// 有线
// const service = createXHR('http://10.2.14.167:8080/');
// const service = createXHR('http://2280565n60.goho.co/');

export default service;
