/*
 * @Description: 
 * @Author: wanjikun
 * @Date: 2021-05-01 12:08:55
 * @LastEditTime: 2021-05-02 14:29:48
 * @LastEditors: wanjikun
 */
import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: () => import( /* webpackChunkName: "Home" */ '@/views/Home.vue')
  },
  // 晚宴
  {
    path: '/wanyan',
    name: 'Wanyan',
    component: () => import( /* webpackChunkName: "Wanyan" */ '@/views/Wanyan.vue')
  },
  // 分会
  {
    path: '/fenhui',
    name: 'Fenhui',
    component: () => import( /* webpackChunkName: "Fenhui" */ '@/views/Fenhui.vue')
  },
  // 主会
  {
    path: '/zhuhui',
    name: 'Zhuhui',
    component: () => import( /* webpackChunkName: "Zhuhui" */ '@/views/Zhuhui.vue')
  },
  // 攻略
  {
    path: '/strategy',
    name: 'Strategy',
    component: () => import( /* webpackChunkName: "Strategy" */ '@/views/Strategy.vue')
  },
  {
    path: '/zhaozhuang',
    name: 'Zhaozhuang',
    component: () => import( /* webpackChunkName: "Zhaozhuang" */ '@/views/Zhaozhuang.vue')
  },
  {
    path: '/moments',
    name: 'Moments',
    component: () => import( /* webpackChunkName: "Moments" */ '@/views/Moments.vue')
  },
  {
    path: '/handlebook',
    name: 'Handlebook',
    component: () => import( /* webpackChunkName: "Handlebook" */ '@/views/Handlebook.vue')
  },
  {
    path: '/pdf',
    name: 'Pdf',
    component: () => import( /* webpackChunkName: "Pdf" */ '@/views/Pdf.vue')
  },
  {
    path: '/address',
    name: 'address',
    component: () => import( /* webpackChunkName: "address" */ '@/views/address.vue')
  },
  {
    path: '/video',
    name: 'video',
    component: () => import( /* webpackChunkName: "video" */ '@/views/video.vue')
  },
  // {
  //   path: '/about',
  //   name: 'About',
  //   // route level code-splitting
  //   // this generates a separate chunk (about.[hash].js) for this route
  //   // which is lazy-loaded when the route is visited.
  //   component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  // }
]

const router = new VueRouter({
  routes
})

export default router
