import request from '../common/request';
import request2 from '../common/request2';
class RequestIndex {
  add(formData) {
    return request.post('/wechat/Invitation/add', formData)
  }
  getWxConfig(formData) {
    return request2.post('/mp/api/get', formData)
  }
  SaveMailingAddress(formData) {
    return request.post('/wechat/Invitation/SaveMailingAddress', formData)
  }
  GetMailingAddress(formData) {
    return request.post('/wechat/Invitation/GetMailingAddress', formData)
  }
}


export default new RequestIndex();