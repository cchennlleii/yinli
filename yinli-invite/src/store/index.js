import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    isShowMiusic: true
  },
  mutations: {
    setisShowMiusic (state, payload){
      state.isShowMiusic = payload
    }
  },
  actions: {
  },
  modules: {
  }
})
